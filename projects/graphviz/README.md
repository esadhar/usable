---
project_name: Graphviz
version: 2.38
current_version_release_date: 2014-04-13
# Dates are in universal format: YYYY-MM-DD
edition_date:
project_first-release:
# Is the project developpement still active?
still_active: true
license: open source, free
taxonomy:
  creators: [AT&T]
  plateforms: [linux, windows, mac os x, web]
  categories: [Pictures]
  tags: [mind mapping]
project_url: http://www.graphviz.org/About.php
extra_links:
  youtube:
  forum:
  demos:
  tutorials: http://cyberzoide.developpez.com/graphviz/
technology: C
dependency:
installation_process: >
  - Step 1
  - Step 2
# Boolean value: 'true' or 'false'
installation_needs_command_line: false
# Operating system used for the test
tested_on: Windows 7
---

## Short Description

Markdown is allowed here. The short description describes the project briefly, it should not be longer than one or two sentences.

## Detailed Description

This is the long description, it can be as long as you need. Describing the project and your experience in details. It should also briefly describes how the attached pictures / video / sounds has been made.
