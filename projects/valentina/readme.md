---
project_name: Valentina
version: 3.3
# Is the project developpement still active?
still_active: true
license:  GNU General Public License version 3
taxonomy:
  creators: [ Roman Telezhynskyi]
  plateforms: [GNU Linux, Windows, Mac]
  categories: [Technical drawing]
  tags:
project_url: http://valentinaproject.bitbucket.org/
extra_links:
  youtube: https://www.youtube.com/channel/UCA6X0x1kfpjl38xIp1KX4Nw
  forum: https://groups.google.com/forum/#!forum/valentina-project-list
  tutorials: https://bytebucket.org/dismine/valentina/wiki/docs/Translations/French/Mode_emploi_programme_Valentina_FR_20150114.pdf?rev=76c7216fb01c8f00742d917475bf6bc34cbeaef7
technology: c++
# Operating system used for the test
tested_on: Windows 10
---

## Short Description

Valentina is an open source pattern drafting software tool.
## Detailed Description
Valentina is a free software that alow clothes designers to create with freedom. The idea of this software is to offer to new clothes designers a free and complete software, because in this industrie almost every software are expensive or free but not complete. This software is extremelly precise, you have all the data you need to create any sewing pattern.
