---
project_name: FontForge
version:
# Is the project developpement still active?
still_active: true
license:  GNU GPL
taxonomy:
  creators: [ George Williams]
  plateforms: [GNU Linux, Windows, Mac]
  categories: [Font editor ]
  tags:
project_url: https://fontforge.github.io/en-US/
extra_links:
  youtube: http://fr.flossmanuals.net/fontes-libres/presentation-de-fontforge/
  tutorials: http://designwithfontforge.com/en-US/index.html
  tutirials: http://fr.flossmanuals.net/fontes-libres/presentation-de-fontforge/

technology: C
# Operating system used for the test
tested_on: Windows 10
---

## Short Description
FontForge is a full-featured font editor which supports all common font formats.
## Detailed Description
FontForge allows you to modify any font file and edit any character included in it, manually. Besides that, the user can see a preview of his modification in real time. Furthermore, the program can edit existing font files or it can create new fonts from scratch. But some information displayed in the Font Info tab may not be understood by some computer users. 
