---
project_name: Bird Font
version: 2.13
current_version_release_date: 2015-11-25
# Dates are in universal format: YYYY-MM-DD
edition_date: 2015-11-25
project_first-release:
# Is the project developpement still active?
still_active: true
license: GNU
taxonomy:
  creators: [Johan Mattsson, Marko Jovanovac]
  plateforms: [windows, Os, Linux, Open BSD]
  categories: [font editor]
  tags: []
project_url: https://birdfont.org/
extra_links:
  youtube: https://www.youtube.com/watch?v=_V2hlPMtSMg
technology:
dependency:
installation_process: >
  - Step Up
# Boolean value: 'true' or 'false'
installation_needs_command_line: false
# Operating system used for the test
tested_on: Windows 10
---

## Short Description
BirdFont is a font editor which lets you create vector graphics and export fonts in TTF, EOT and SVG format.

## Detailed Description
With Birdfont you can create your font or import .svg and edit your font. When you create your font directly in Birdfont, the tools you use are not really intuitive.
But if you are familiare with this quind of tools, there will be no trouble for you.
