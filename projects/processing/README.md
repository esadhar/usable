---
project_name: Processing
version: 3.0.1
current_version_release_date: 2015-10-23
# Dates are in universal format: YYYY-MM-DD
edition_date: 2002-12-14
project_first-release: 2002-12-14
# Is the project developpement still active?
still_active: true
license: GNU GPL
taxonomy:
  creators: [Ben Fry and Casey Reas]
  plateforms: [linux, windows, mac]
  categories: [Video, Sound, 3D]
  tags: [generator, light, monotask]
project_url: http://example.com/
extra_links:
  youtube: http://example.com/
  forum: http://example.com/
  demos: http://example.com/
  tutorials: http://example.com/
technology: Python
dependency: Python, Pyglet
installation_process: >
  - Step 1
  - Step 2
# Boolean value: 'true' or 'false'
installation_needs_command_line: true
# Operating system used for the test
tested_on: Windows 10
---

## Short Description

Markdown is allowed here. The short description describes the project briefly, it should not be longer than one or two sentences.

## Detailed Description

This is the long description, it can be as long as you need. Describing the project and your experience in details. It should also briefly describes how the attached pictures / video / sounds has been made.
