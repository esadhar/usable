---
project_name: FreeCAD
version: 0.16
current_version_release_date: 2002-12-14
# Dates are in universal format: YYYY-MM-DD
edition_date: 2015-12-10
project_first-release:  2002-
# Is the project developpement still active?
still_active: true
license: LGPL
taxonomy:
  creators: [ Jürgen Riegel, Yorik van Havre]
  plateforms: [lwindows, ubuntu, mac]
  categories: [3D]
  tags: [ ]
project_url: http://www.freecadweb.org/
extra_links:
  youtube: https://www.youtube.com/results?search_query=freecad
  forum: http://forum.freecadweb.org/
  demos: http://www.freecadweb.org/wiki/?title=Screenshots
  tutorials: http://www.freecadweb.org/wiki/index.php?title=Category:Tutorials
technology: Python, c++
dependency:
installation_process: >
  - Step Up
# Boolean value: 'true' or 'false'
installation_needs_command_line: false
# Operating system used for the test
tested_on: Windows 10
---

## Short Description

FreeCAD is a 3D CAD/CAE parametric modeling application. It is primarily made for mechanical design, but also serves all other uses where you need to model 3D objects with precision and control over modeling history.
## Detailed Description

FreeCAD is still in the early stages of development, so, although it already offers you a large (and growing) list of features, much is still missing, specially comparing it to commercial solutions, and you might not find it developed enough yet for use in production environment. Still, there is a fast-growing community of enthusiastic users, and you can already find many examples of quality projects developed with FreeCAD.
