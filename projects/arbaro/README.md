---
project_name: Arbaro
version: 1.9.8
current_version_release_date: 2015-02-03
# Dates are in universal format: YYYY-MM-DD
edition_date: 2015-12-03
project_first-release:  2003-09-06
# Is the project developpement still active?
still_active: true
license: GNU GPL 2.0 (GPLv2)
taxonomy:
  creators: [Unknow for now]
  plateforms: [linux, windows, mac]
  categories: [3D Modeling, 3D Rendering]
  tags: [generator]
project_url: http://arbaro.sourceforge.net/
extra_links:
  youtube:
  forum:
  demos:
  tutorials: https://www.youtube.com/watch?v=sX3cqdCmmfE
technology: Java
dependency: Python, Java
installation_process: >
  - Step 1
  - Step 2
# Boolean value: 'true' or 'false'
installation_needs_command_line: false
# Operating system used for the test
tested_on: Windows 10
---

## Short Description

Arbaro is a tree generator, compatible with blender (export Wavefront OBJ files). Very easy to use because of the little picture on the down right corner.

## Detailed Description

No detailed description yet
