---
project_name: Freeplane
version: 1.3.15
current_version_release_date: 2015.02.2
# Dates are in universal format: YYYY-MM-DD
edition_date: 2015-11-30
project_first-release: 2009.07.01
# Is the project developpement still active?
still_active: true
license: GNU GPL
taxonomy:
  creators: [Dimitry Polivaev,Volker Boerchers,Felix Natter]
  plateforms: [linux, windows, mac]
  categories: [Mind Map]
  tags: []
project_url: http://freeplane.sourceforge.net/wiki/index.php/Main_Page
extra_links:
  demos: http://www.freemindparlexemple.fr/
  forum:http: //sourceforge.net/p/freeplane/discussion/758437

technology: Java
dependency: FreeMind
installation_process: >
  - Executable
# Boolean value: 'true' or 'false'
installation_needs_command_line: false
# Operating system used for the test
tested_on: Windows 10
---

## Short Description

Freeplane is a free and open source software application for bulding mind maps.

## Detailed Description

Freeplane aims for maximum ease and speed of use. Occupying the middle ground between an editor and a diagramming tool, Freeplane allows the user to add content as quickly and naturally as they would in a text editor, yet producing structured content that can be manipulated as easily as a diagram. The workflow is unimpeded by the need to think about and manually place each piece of information; the user can intuitively input content as paragraphs and headings, and easily reorganise at a later stage. The width and other aspect of each topic (node) can be set independently and a node can be defined to behave indepently from the nodes in the tree structure, to be free positionable.
