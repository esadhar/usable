---
project_name: Pure Data
version: Extended
current_version_release_date: 2014.11.01
edition_date: 2002-12-14
project_first-release: 90's
# Is the project developpement still active?
still_active: true
license: BSD & Open Source
taxonomy:
  creators: [Miller Puckette ]
  plateforms: [Linux, Mac OS X, iOs, Andro�d & Windows.]
  categories: [sounds, videos, graphics, images & can be use with external sensors.]
  tags: []
project_url: http://puredata.info/
extra_links:
  youtube: https://www.youtube.com/watch?v=rtgGol-I4gA&list=PL12DC9A161D8DC5DC
  forum: http://forum.pdpatchrepo.info/
  tutorials: https://www.youtube.com/watch?v=kYulbNavNJU
technology:
dependency: Max, puredyne
installation_process: > Setup for the extended version
Command line for Vanilla
# Boolean value: 'true' or 'false'
installation_needs_command_line: false
# Operating system used for the test
tested_on: Windows 10
---

## Short Description

Pure Data is a visual programming language. It is possible to create and manipulate video, graphics, images in realtime with extensive possibilities for interactivity with audio & external sensors.

## Detailed Description

This extremely vast software allows you to make sound, the video, graphs without writing line of codes. The language used is relatively simple to understand, even if you are not familiar with coding or processing (if you are, some interactions should be easier to understand even if it's not the same language). You develop your project in a graphic way with objects which you bound to each other, every object as a function. The object can be: create with PureData, abstractions (reusable patches create in Pure Dated), external object create with other software and you can also work with MIDI (from an external keyboard or a software). Pure Dated is relatively addicting when we realize all the possibility it has.
