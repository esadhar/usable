---
project_name: Sculptgl
version:
current_version_release_date: 2002-12-14
# Dates are in universal format: YYYY-MM-DD
edition_date: 2015-12-11
project_first-release: 2002-12-14
# Is the project developpement still active?
still_active: true
license: MIT
taxonomy:
  creators: [Stéphane Ginier]
  plateforms: [web]
  categories: [3D]
  tags: []
project_url: http://stephaneginier.com/sculptgl/
extra_links:
  forum:
  demos: http://stephaneginier.com/gallery
  tutorials:
technology: Java Script, weGL
dependency:
installation_process: >

# Boolean value: 'true' or 'false'
installation_needs_command_line: false
# Operating system used for the test
tested_on: Windows 10
---

## Short Description

SculptGL is a web sculpting application.
## Detailed Description
SculptGL is a free 3D sculpting CAD (Computer Assisted Design) program created by Stephane Ginier, a University of Montréal exchange student. It lets users sculpt 3D designs, apply symmetry, import/export these files in 3D printable formats, and automatically share them on portfolio sites like Sketchfab.
