---
project_name: Stellarium
version: 0.14.1
# Is the project developpement still active?
still_active: unknow
license:   GNU GPL
taxonomy:
  creators: [ Fabien Chéreau]
  plateforms: [GNU Linux, Windows, Mac]
  categories: [Astronomy]
  tags:
project_url: None active
extra_links:
  download: http://www.softpedia.com/get/Others/Home-Education/Stellarium.shtml
  wiki: https://fr.wikipedia.org/wiki/Stellarium
  youtube: https://www.youtube.com/watch?v=4lKLqE3POoo

technology: OpenGL
# Operating system used for the test
tested_on: Windows 10
---

## Short Description

Stellarium is a software of real time astronomy.
## De tailed Description
Stellarium is a powerfull software that aims you to navigate into space. You can also obeserve stars in real time. This software win the gold trophies in the category "Education" of the Trophies of the free. You can control the place of observation, the time, luminosity of the sky, and many others fonctionnality. 
