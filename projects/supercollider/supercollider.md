---
project_name: SuperCollider
version: 3.7
current_version_release_date: 2015-03-22
# Dates are in universal format: YYYY-MM-DD
edition_date: 2015.11.20
project_first-release: 1996
# Is the project developpement still active?
still_active: true
license: GNU
taxonomy:
  creators: [James McCartney]
  plateforms: [linux, window, mac, freeBSD]
  categories: [Sound]
  tags: []
project_url: https://supercollider.github.io/
extra_links:
  forum: http://supercollider.sourceforge.net/community/
  demos: http://sccode.org/
  tutorials: https://www.youtube.com/playlist?list=PLPYzvS8A_rTaNDweXe6PX4CXSGq4iEWYC
technology:
dependency: Synth-O-Matic, MAX
installation_process: >
  - Step Up
# Boolean value: 'true' or 'false'
installation_needs_command_line: false
# Operating system used for the test
tested_on: Windows 10
---

## Short Description

Supercollider is an environment and programming language for real-time audio synthesis and algorithmic composition.

## Detailed Description
With SuperCollider, you make sounds with ligne of codes, the language is relatively simple if you take the time to watch some tutoriel. You can also find alot of code that you an copy and modifiy. The intressting part with this software, is that it provide audio synthesis in a high level language with dynamic typing.
