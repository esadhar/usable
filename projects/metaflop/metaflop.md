---
project_name: Metaflop
version:
current_version_release_date: 2012-
# Dates are in universal format: YYYY-MM-DD
edition_date: 2015-12-08
project_first-release: 2012-
# Is the project developpement still active?
still_active: true
license: GNU
taxonomy:
  creators: [alexis reigel, marco müller]
  plateforms: [ web]
  categories: [Type]
  tags: []
project_url: http://www.metaflop.com/modulator
extra_links:
  conference: https://vimeo.com/113933706
  faq: http://www.metaflop.com/faq
technology: javascript
dependency:
# Boolean value: 'true' or 'false'
installation_needs_command_line: false
# Operating system used for the test
tested_on: Windows 10
---

## Short Description

Metaflop is an easy to use web application for modulating your own fonts.

## Detailed Description

With the modulator it is possible to use metafont without dealing with the programming language by yourself, but simply by changing sliders or numeric values of the font parameter set. this enables you to focus on the visual output – adjusting the parameters of the typeface to your own taste. all the repetitive tasks are automated in the background. the unique results can be downloaded as a webfont package for embedding on your homepage or an opentype postscript font (otf) which can be used on any system in any application supporting otf.
