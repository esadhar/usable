---
project_name: Make 8bit art
version: 1
current_version_release_date: 2015-
# Dates are in universal format: YYYY-MM-DD
edition_date: 2015-12-19
project_first-release: 2013-
# Is the project developpement still active?
still_active: true
license:
taxonomy:
  creators: [Jenn Schiffer]
  plateforms: [web]
  categories: [generator]
  tags: []
project_url: http://make8bitart.com/

technology:  JavaScript, HTML, CSS
dependency: Python, Pyglet
# Boolean value: 'true' or 'false'
installation_needs_command_line: false
# Operating system used for the test
tested_on: Windows 10
---

## Short Description

make8bitart.com is a web-based pixel art application.
## Detailed Description

With make8bitart you can draw and paint in 8 bit, but you can also save it or import images you like. It is really simple to use and fun !
