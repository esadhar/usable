<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Usable</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="font/stylesheet.css">
  </head>
  <body>
    <header>
      <h1>usable</h1>
      <p><a href="https://gitlab.com/esadhar/usable/">Repository</a> <a href="https://gitlab.com/esadhar/usable/wikis/liste-outils">Full projects list</a> <a href="https://gitlab.com/esadhar/usable/issues">Issues</a></p>
    </header>
  <?php
  // Include Libs and Functions
  require_once "spyc.php";
  require_once "functions.php";
  // New yaml instance
  $spyc = new Spyc();
  // Recursively read the chosen directory for yaml content in .md
  $directory = "../projects/";
  $ignored = "../projects/README.md";
  $projects = rglob($directory."{*.md}", GLOB_BRACE);
  $projectNbr = 0;
  // Sort projects, most recent first
  usort($projects, create_function('$a,$b', 'return filemtime($b) - filemtime($a);'));

  foreach ($projects as $project) {
    $projectNbr ++;
    if ($project == $ignored) {
      return;
    }
    $y = Spyc::YAMLLoad($project);
  ?>
  <section>
    <header>
      <h1><?php echo $y['project_name']; ?></h1>
      <a class="project-url" href="<?php echo $y['project_url']; ?>">Project’s website</a>
      <span class="show-meta button">Show Meta info</span>
      <ul class="meta">
        <li>Version: <?php echo $y['version']; ?></li>
        <li><?php if ($y['version'] === "true" ) {echo "Looks still active";} ?></li>
        <li>License: <?php echo $y['license']; ?></li>
        <li>Tested on: <?php echo $y['tested_on']; ?></li>
      </ul>
    </header>
    <div class="short-description">
      <?php echo $y['0']; ?>
    </div>
    <div class="long-description">
      <?php echo $y['1']; ?>
    </div>
    <div class="videos">
      <?php
      $viddir = dirname($project)."/videos/"; // need to be polished
      $videos = glob($viddir."{*.mp4}", GLOB_BRACE);
      foreach ($videos as $video) {
        $vidName = basename($video, ".mp4");
        $vidMp4 = $viddir."/".$vidName.".mp4";
        ?>
        <video controls preload="metadata">
          <?php if ($vidMp4): ?>
          <source src="<?php echo $vidMp4 ?>" type="video/mp4">
          <?php endif; ?>
        </video>
        <?php
      }
      ?>
    </div>
    <div class="images">
      <?php
      $imgdir = dirname($project)."/images/"; // need to be polished
      $images = glob($imgdir."{*.jpg,*.jpeg,*.gif,*.png,*.svg}", GLOB_BRACE);
      foreach ($images as $image) {
        echo "<img src='".$image."' />";
      }
      ?>
    </div>
  </section>
  <?php } ?>
  </body>
  <script src="js/jquery-1.11.3.min.js"></script>
  <script src="js/main.js"></script>
</html>
